#include "../include/formatting.h"

void str_concat(char** str, const char * str2) {
    char* str1 = *str;
    int first_len = 0;
    int second_len = 0;
    while (str1[first_len] != '\0') {
        first_len++;
    }
    while (str2[second_len] != '\0') {
        second_len++;
    }
    char * new_str = malloc(sizeof(char) * (first_len + second_len + 1));
    for (int i = 0; i < first_len; i++) {
        new_str[i] = str1[i];
    }
    for (int i = first_len; i < first_len + second_len; i++) {
        new_str[i] = str2[i - first_len];
    }
    new_str[first_len + second_len] = '\0';
    free(str1);
    *str = new_str;
}


char* select_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "SELECT {\n");
    char* r;

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "ENTITY: ");
    str_concat(&res, node->additional_content.name_of_something);    
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "COLUMNS: ");
    if (node->first_node != NULL) { // если указан список колонок
        str_concat(&res, "\n");
        r = to_string(node->first_node, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "*");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "TABLE: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");

    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "FILTER CONDITION: ");
    if (node->second_node != NULL) { // если указан where
        str_concat(&res, "\n");
        r = to_string(node->second_node, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NO FILTER");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}


char* join_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "JOIN {\n");
    char* r;

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "LEFT ENTITY: ");
    str_concat(&res, node->additional_content.name_of_something);    
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "LEFT TABLE: ");
    str_concat(&res, node->main_content.name_of_something);    
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "ON: ");

    for (int i = 0; i < tabs + 2; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "RIGHT ENTITY: ");
    str_concat(&res, node->additional_content_second.name_of_something);    
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 2; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "RIGHT TABLE: ");
    str_concat(&res, node->main_content_second.name_of_something);    
    str_concat(&res, "\n");

    r = to_string(node->first_node, tabs + 2);
    str_concat(&res, r);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 2; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "EQUALS: ");
    r = to_string(node->second_node, tabs + 3); // right column
    str_concat(&res, r);
    str_concat(&res, "\n");

    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "SELECT: ");
    r = to_string(node->third_node, tabs + 2); //columns from join
    str_concat(&res, r);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* delete_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "DELETE {\n");
    char* r;

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "ENTITY: ");
    str_concat(&res, node->additional_content.name_of_something);    
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "TABLE: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");

    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "FILTER CONDITION: ");
    if (node->first_node != NULL) { // если указан where
        str_concat(&res, "\n");
        r = to_string(node->first_node, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "ALL ROWS");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* update_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "UPDATE {\n");
    char* r;

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "ENTITY: ");
    str_concat(&res, node->additional_content.name_of_something);    
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "COLUMNS: ");
    str_concat(&res, "\n");
    r = to_string(node->first_node, tabs + 2);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "VALUES: ");
    str_concat(&res, "\n");
    r = to_string(node->third_node, tabs + 2);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "TABLE: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");


    str_concat(&res, "\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "FILTER CONDITION: ");
    if (node->second_node != NULL) { // если указан where
        str_concat(&res, "\n");
        r = to_string(node->second_node, tabs + 2);
        str_concat(&res, r);
        free(r);
    } else {
        str_concat(&res, "NO FILTER");
    }
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* insert_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "INSERT {\n");
    char* r;


    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "TABLE: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");


    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "VALUES: ");
    str_concat(&res, "\n");
    r = to_string(node->first_node, tabs + 2);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* create_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "CREATE {\n");
    char* r;


    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "TABLE: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");


    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "COLUMNS: ");
    str_concat(&res, "\n");
    r = to_string(node->first_node, tabs + 2);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* drop_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "DROP {\n");
    char* r;


    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "TABLE: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}


char* filter_cmp_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "COMPARE {\n");
    char* r = to_string(node->first_node, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, compare_type_strings[node->main_content.cmp_type]);


    str_concat(&res, "\n");
    r = to_string(node->second_node, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* filter_logic_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, " {\n");
    char* r = to_string(node->first_node, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    

    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, logic_operator_strings[node->main_content.logic_op]);


    str_concat(&res, "\n");
    r = to_string(node->second_node, tabs + 1);
    str_concat(&res, r);
    free(r);
    str_concat(&res, "\n");
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}


char* new_column_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "COLUMN {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }

    str_concat(&res, "NAME: ");
    str_concat(&res, node->main_content.name_of_something);
    str_concat(&res, "\n");

    str_concat(&res, "\n");
    char* r = to_string(node->first_node, tabs + 1); // type
    str_concat(&res, r);
    free(r);
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* old_column_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "COLUMN {\n");
    for (int i = 0; i < tabs + 1; i++) {
        str_concat(&res, "   ");
    }

    str_concat(&res, "ENTITY: ");
    str_concat(&res, node->main_content.name_of_something);    
    str_concat(&res, "\n");

    str_concat(&res, "NAME: ");
    str_concat(&res, node->additional_content.name_of_something);
    str_concat(&res, "\n");

    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}


char* list_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "LIST {\n");
    struct ast_node* next = node;
    char* r;
    while (next != NULL) {
        r = to_string(next->first_node, tabs + 1);
        str_concat(&res, r);
        free(r);
        str_concat(&res, ";\n");
        next = next->second_node;
    }
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "}");
    return res;
}

char* string_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "STRING { ");
    str_concat(&res, node->main_content.string_value);
    str_concat(&res, " }");
    return res;
}

char* integer_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "INTEGER { ");
    int length = snprintf(NULL, 0, "%d", node->main_content.integer_value);
    char* str = malloc( length + 1 );
    snprintf(str, length + 1, "%d", node->main_content.integer_value);
    str_concat(&res, str);
    free(str);
    str_concat(&res, " }");
    return res;
}

char* float_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "FLOAT { ");
    int length = snprintf(NULL, 0, "%f", node->main_content.float_value);
    char* str = malloc( length + 1 );
    snprintf(str, length + 1, "%f", node->main_content.float_value);
    str_concat(&res, str);
    free(str);
    str_concat(&res, " }");
    return res;
}

char* Bool_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "BOOLEAN { ");
    str_concat(&res, node->main_content.boolean_value ? "TRUE" : "FALSE");
    str_concat(&res, " }");
    return res;
}

char* type_to_string(struct ast_node* node, int tabs) {
    char* res = malloc(sizeof(char));
    res[0] = '\0';
    for (int i = 0; i < tabs; i++) {
        str_concat(&res, "   ");
    }
    str_concat(&res, "DATA TYPE { ");
    str_concat(&res, data_type_strings[node->main_content.d_type]);
    str_concat(&res, " }");
    return res;
}


char* to_string(struct ast_node* node, int tabs) {
    switch (node->type) {
        case SELECT_QUERY:
            return select_to_string(node, tabs);
            break;
        case JOIN_QUERY:
            return join_to_string(node, tabs);
            break;
        case DELETE_QUERY:
            return delete_to_string(node, tabs);
            break;
        case UPDATE_QUERY:
            return update_to_string(node, tabs);
            break;
        case INSERT_QUERY:
            return insert_to_string(node, tabs);
            break;
        case CREATE_QUERY:
            return insert_to_string(node, tabs);
            break;
        case DROP_QUERY:
            return drop_to_string(node, tabs);
            break;
        case FILTER_CMP:
            return filter_cmp_to_string(node, tabs);
            break;
        case FILTER_LOGIC:
            return filter_logic_to_string(node, tabs);
            break;
        case CREATE_COLUMN:
            return new_column_to_string(node, tabs);
            break;
        case OLD_COLUMN:
            return old_column_to_string(node, tabs);
            break;
        case CREATE_LIST:
            return list_to_string(node, tabs);
            break;
        
    }        
}

void print_tree(struct ast_node* root_node) {
    if (root_node == NULL) {
        printf("No root\n");
        return;
    }
    char* res = to_string(root_node, 0);
    printf("%s\n", res);
    free(res);
}
