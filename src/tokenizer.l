%option noyywrap caseless yylineno nounput noinput batch debug

%{
    #include <stdio.h>
    #include <string.h>
    #include "parser.tab.h"
    #include "../include/tree.h"

    void print_tokenizer_error(char*);
%}

alpha   [A-Za-z_0-9 \t\r]
id      [a-zA-Z][a-zA-Z_0-9]*
int     ([-+]?[0-9])+
float   [+-]?([0-9]*[.])?[0-9]+
blank   [ \t\r]
word    ([a-zA-Z_][a-zA-Z0-9_]*)
quoted_string \"{word}*\"

%%

"SELECT"    {return SELECT;}
"JOIN"      {return JOIN;}
"DELETE"    {return DELETE;}
"UPDATE"    {return UPDATE;}
"INSERT"    {return INSERT;}
"CREATE"    {return CREATE;}
"DROP"      {return DROP;}

"FROM"      {return FROM;}
"IN"        {return IN;}
"WHERE"     {return WHERE;}
"ON"        {return ON;}
"EQUALS"    {return EQUALS;}
"SET"       {return SET;}

"&&"        {yylval.logic_op = 0; return AND;}
"||"        {yylval.logic_op = 1; return OR;}

"TRUE"      {yylval.boolean = 1; return BOOL;}
"FALSE"     {yylval.boolean = 0; return BOOL;}

"CONTAINS"  {yylval.cmp_type = 0; return CMP;}
">"         {yylval.cmp_type = 1; return CMP;} 
">="        {yylval.cmp_type = 2; return CMP;}
"<"         {yylval.cmp_type = 3; return CMP;}
"<="        {yylval.cmp_type = 4; return CMP;}
"=="        {yylval.cmp_type = 5; return CMP;}
"!="        {yylval.cmp_type = 6; return CMP;}

"STRING"    {yylval.type = 0; return TYPE;}
"INT"       {yylval.type = 1; return TYPE;}
"FLOAT"     {yylval.type = 2; return TYPE;}
"BOOL"      {yylval.type = 3; return TYPE;}

"("         {return LB;}
")"         {return RB;}
","         {return COMMA;}
"."         {return DOT;}
";"         {return ENDQUERY;}
"\""        {return QUOTE;}

{word}      {
    sscanf(yytext, "%s", yylval.string);
    return (STR);
}
{int}       {
    yylval.integer = atoi(yytext);
    return (INT);
}
{float}     {
    yylval.float_v = atof(yytext);
    return (FLOAT);
}

[ \t]   { /* ignore */ }
[\n]    {}
.       {
    print_tokenizer_error(yytext);
    return (OTHER);
}
%%

void print_tokenizer_error(char* token) {
    printf("Error in tokenizer while reading token = %s \n", token);
}
