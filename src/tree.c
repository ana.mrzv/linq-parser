#include "../include/tree.h"

struct ast_node* new_select_query(const char* table_name, const char* entity_name, struct ast_node* column_list, struct ast_node* filter_statement) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = SELECT_QUERY;
    strcpy(node->main_content.name_of_something, table_name);
    strcpy(node->additional_content.name_of_something, entity_name);
    node->first_node = column_list;
    node->second_node = filter_statement;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_join_query(const char* left_table_name, const char* left_entity_name, const char* right_table_name, const char* right_entity_name, struct ast_node* left_column, struct ast_node* right_column, struct ast_node* column_list) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = JOIN_QUERY;

    strcpy(node->main_content.name_of_something, left_table_name);
    strcpy(node->additional_content.name_of_something, left_entity_name);

    strcpy(node->main_content_second.name_of_something, right_table_name);
    strcpy(node->additional_content_second.name_of_something, right_entity_name);

    node->first_node = left_column;
    node->second_node = right_column;
    node->third_node = column_list;
    return node;
}

struct ast_node* new_delete_query(const char* table_name, const char* entity_name, struct ast_node* filter_statement) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = DELETE_QUERY;
    strcpy(node->main_content.name_of_something, table_name);
    strcpy(node->additional_content.name_of_something, entity_name);
    node->first_node = filter_statement;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_update_query(const char* table_name, const char* entity_name, struct ast_node* column_list, struct ast_node* filter_statement, struct ast_node* value_list) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = UPDATE_QUERY;
    strcpy(node->main_content.name_of_something, table_name);
    strcpy(node->additional_content.name_of_something, entity_name);
    node->first_node = column_list;
    node->second_node = filter_statement;
    node->third_node = value_list;
    return node;
}

struct ast_node* new_insert_query(const char* table_name, struct ast_node* value_list) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = INSERT_QUERY;
    strcpy(node->main_content.name_of_something, table_name);
    node->first_node = value_list;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_create_query(const char* table_name, struct ast_node* initialize_column_list) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = CREATE_QUERY;
    strcpy(node->main_content.name_of_something, table_name);
    node->first_node = initialize_column_list;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_drop_query(const char* table_name) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = DROP_QUERY;
    strcpy(node->main_content.name_of_something, table_name);
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_filter_compare_statement(enum compare_type cmp_type, struct ast_node* first_operand, struct ast_node* second_operand) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = FILTER_CMP;
    node->main_content.cmp_type = cmp_type;
    node->first_node = first_operand;
    node->second_node = second_operand;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_filter_logic_statement(enum logic_operator logic_op, struct ast_node* first_operand, struct ast_node* second_operand) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = FILTER_LOGIC;
    node->main_content.logic_op = logic_op;
    node->first_node = first_operand;
    node->second_node = second_operand;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_column(const char* column_name, struct ast_node* type) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = CREATE_COLUMN;
    strcpy(node->main_content.name_of_something, column_name);
    node->first_node = type;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* old_column(const char* entity_name, const char* column_name) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = OLD_COLUMN;
    strcpy(node->main_content.name_of_something, entity_name);
    strcpy(node->additional_content.name_of_something, column_name);
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_list(struct ast_node* first_part, struct ast_node* second_part) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = CREATE_LIST;
    node->first_node = first_part;
    node->second_node = second_part;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_string(const char* string_val) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = STRING_NODE;
    strcpy(node->main_content.string_value, string_val);
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_integer(int int_val) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = INTEGER_NODE;
    node->main_content.integer_value = int_val;
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_float(float float_val) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = FLOAT_NODE;
    node->main_content.float_value = float_val;
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_boolean(bool bool_val) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = BOOLEAN_NODE;
    node->main_content.boolean_value = bool_val;
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

struct ast_node* new_type(enum data_type dtype) {
    struct ast_node* node = malloc(sizeof(struct ast_node));
    node->type = TYPE_NODE;
    node->main_content.d_type = dtype;
    node->first_node = NULL;
    node->second_node = NULL;
    node->third_node = NULL;
    return node;
}

