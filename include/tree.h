#ifndef TREE_H
#define TREE_H

#include <stdbool.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>

enum node_type
{
    SELECT_QUERY,
    JOIN_QUERY,
    DELETE_QUERY,
    UPDATE_QUERY,
    INSERT_QUERY,
    CREATE_QUERY,
    DROP_QUERY,

    FILTER_CMP,
    FILTER_LOGIC,

    CREATE_COLUMN,
    CREATE_LIST,
    OLD_COLUMN,

    INTEGER_NODE,
    STRING_NODE,
    FLOAT_NODE,
    BOOLEAN_NODE,

    NAME,
    TYPE_NODE
};

enum data_type
{
    STRING_TYPE,
    INTEGER_TYPE,
    FLOAT_TYPE,
    BOOLEAN_TYPE,
};

static const char* data_type_strings[] = {
	"STRING",
    "INTEGER",
    "FLOAT",
    "BOOLEAN"
};

enum logic_operator
{
    AND_OPERATOR,
    OR_OPERATOR,
};

static const char* logic_operator_strings[] = {
	"AND",
    "OR"
};

enum compare_type
{
    CONTAINS_SUBSTRING,
    GREATER,
    GREATER_OR_EQUALS,
    LESS,
    LESS_OR_EQUALS,
    EQUALS_STRICT,
    NOT_EQUALS,
};

static const char* compare_type_strings[] = {
	"CONTAINS_SUBSTRING",
    "GREATER",
    "GREATER_OR_EQUAL",
    "LESS",
    "LESS_OR_EQUALS",
    "EQUALS_STRICT",
    "NOT_EQUALS"
};


union content
{
    char *name_of_something;
    enum data_type d_type;
    enum logic_operator logic_op;
    enum compare_type cmp_type;

    char *string_value;
    int integer_value;
    float float_value;
    bool boolean_value;
};

struct ast_node
{
    enum node_type type;
    union content main_content;
    union content additional_content;

    union content main_content_second;
    union content additional_content_second;


    struct ast_node *first_node;
    struct ast_node *second_node;
    struct ast_node *third_node;
};

struct ast_node* new_select_query(const char* table_name, const char* entity_name, struct ast_node* column_list, struct ast_node* filter_statement);
struct ast_node* new_join_query(const char* left_table_name, const char* left_entity_name, const char* right_table_name, const char* right_entity_name, struct ast_node* left_column, struct ast_node* right_column, struct ast_node* column_list);
struct ast_node* new_delete_query(const char* table_name, const char* entity_name, struct ast_node* filter_statement);
struct ast_node* new_update_query(const char* table_name, const char* entity_name, struct ast_node* column_list, struct ast_node* filter_statement, struct ast_node* value_list);
struct ast_node* new_insert_query(const char* table_name, struct ast_node* value_list);
struct ast_node* new_create_query(const char* table_name, struct ast_node* initialize_column_list);
struct ast_node* new_drop_query(const char* table_name);
struct ast_node* new_filter_compare_statement(enum compare_type cmp_type, struct ast_node* first_operand, struct ast_node* second_operand);
struct ast_node* new_filter_logic_statement(enum logic_operator logic_op, struct ast_node* first_operand, struct ast_node* second_operand);
struct ast_node* new_column(const char* column_name, struct ast_node* type);
struct ast_node* old_column(const char* entity_name, const char* column_name);
struct ast_node* new_value(const char* column_name, struct ast_node* type);
struct ast_node* new_list(struct ast_node* first_part, struct ast_node* second_part);
struct ast_node* new_string(const char* string_val);
struct ast_node* new_integer(int int_val);
struct ast_node* new_float(float float_val);
struct ast_node* new_boolean(bool bool_val);
struct ast_node* new_type(enum data_type dtype);
void print_tree(struct ast_node* root_node);

#endif