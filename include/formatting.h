#ifndef FORMATTING_H
#define FORMATTING_H


#include "tree.h"

char* to_string(struct ast_node* node, int tabs);

#endif