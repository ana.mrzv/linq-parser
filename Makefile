all: node.h node.c printer.h printer.c utils.h utils.c parser.y tokenizer.l
	make clean
	make program
	make run

program:
	bison -d parser.y
	flex tokenizer.l
	gcc -g -o main parser.tab.c lex.yy.c tree.c formatting.c

run:
	./program

clean:
	rm -f program lex.yy.c parser.tab.c parser.tab.h
